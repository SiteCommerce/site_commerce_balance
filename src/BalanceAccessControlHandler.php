<?php

namespace Drupal\site_commerce_balance;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Balance entity.
 *
 * @see \Drupal\site_commerce_balance\Entity\Balance.
 */
class BalanceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'site commerce view balance entities');

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'site commerce edit balance entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'site commerce delete balance entities');
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'site commerce add balance entities');
  }
}
