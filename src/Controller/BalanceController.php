<?php

namespace Drupal\site_commerce_balance\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_commerce_balance\Entity\Balance;

/** @package Drupal\site_commerce_balance\Controller */
class BalanceController extends ControllerBase {

  /**
   * Создает балансовый аккаунт для пользователя.
   * @param int $uid
   * @param string $type
   * @param float|int $number
   * @param string $currency_code
   * @return void
   */
  public function createBalance(int $uid, string $type = 'currency_account', float $number = 0, string $currency_code = '') {
    // Получаем код валюты по умолчанию.
    if (!preg_match('/^\d{3}$/i', $currency_code)) {
      $config = \Drupal::config('site_commerce_order.settings');
      $currency_code = $config->get('default_currency_code') ? $config->get('default_currency_code') : 'RUB';
    }

    $balance = Balance::create([
      'type' => $type,
      'uid' => $uid,
      'balance' => [
        'number' => $number,
        'currency_code' => $currency_code,
      ],
    ]);
    $balance->save();
    if ($balance instanceof Balance) {
      return $balance;
    }

    return FALSE;
  }

  /**
   * Получает текущий баланс пользователя по выбранному счету.
   * @param int $uid
   * @param string $type
   * @param string $currency_code
   * @return Balance|false
   */
  public function getBalance(int $uid = 0, string $type = 'currency_account', string $currency_code = '') {
    // Если не передан идентификатор пользователя.
    if (!$uid) {
      $uid = \Drupal::currentUser()->id();
    }

    // Получаем код валюты по умолчанию.
    if (!preg_match('/^\d{3}$/i', $currency_code)) {
      $config = \Drupal::config('site_commerce_order.settings');
      $currency_code = $config->get('default_currency_code') ? $config->get('default_currency_code') : 'RUB';
    }

    $result = \Drupal::entityTypeManager()
        ->getStorage('site_commerce_balance')
        ->loadByProperties([
          'uid' => $uid,
          'type' => $type,
          'balance__currency_code' => $currency_code
        ]);
    $balance = reset($result);
    if ($balance instanceof Balance) {
      return $balance;
    }

    return FALSE;
  }
}
