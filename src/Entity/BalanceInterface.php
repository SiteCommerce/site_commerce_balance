<?php

namespace Drupal\site_commerce_balance\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Balance entities.
 *
 * @ingroup site_commerce_balance
 */
interface BalanceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the balance value.
   *
   * @return float
   */
  public function getBalanceNumber();

  /**
   * Sets the balance value.
   *
   * @param float $number
   *
   * @return \Drupal\site_commerce_balance\Entity\BalanceInterface
   */
  public function setBalanceNumber($number);

  /**
   * Gets the currency code of balance.
   *
   * @return string
   */
  public function getBalanceСurrencyСode();

  /**
   * Gets the balance creation timestamp.
   *
   * @return int
   *   Creation timestamp of the balance.
   */
  public function getCreatedTime();

  /**
   * Sets the balance creation timestamp.
   *
   * @param int $timestamp
   *   The Balance creation timestamp.
   *
   * @return \Drupal\site_commerce_balance\Entity\BalanceInterface
   *   The called Balance entity.
   */
  public function setCreatedTime($timestamp);

}
