<?php

namespace Drupal\site_commerce_balance\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Balance type entity.
 *
 * @ConfigEntityType(
 *   id = "site_commerce_balance_type",
 *   label = @Translation("Type the user's balance"),
 *   bundle_of = "site_commerce_balance",
 *   config_prefix = "site_commerce_balance_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class BalanceType extends ConfigEntityBundleBase implements BalanceTypeInterface {

  /**
   * The Balance type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Balance type label.
   *
   * @var string
   */
  protected $label;

}
