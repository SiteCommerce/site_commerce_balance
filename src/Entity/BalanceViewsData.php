<?php

namespace Drupal\site_commerce_balance\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Balance entities.
 */
class BalanceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }
}
