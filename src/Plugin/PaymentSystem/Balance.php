<?php

namespace Drupal\site_commerce_balance\Plugin\PaymentSystem;

use Drupal\site_payments\PaymentSystemPluginBase;

/**
 * @PaymentSystem(
 *   id="site_commerce_balance",
 *   label = @Translation("Balance"),
 *   payment_method_name = @Translation("Current account in your merchant profile")
 * )
 */
class Balance extends PaymentSystemPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaymentSettings() {
    $config = \Drupal::config('site_commerce_balance.settings');
    return [
      'allow_select' => (int) $config->get('allow_select'),
      'block_rules_payment_system' => (int) $config->get('block_rules_payment_system'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus() {
    $settings = $this->getPaymentSettings();
    return (bool) $settings['allow_select'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRulesPaymentSystem() {
    $settings = $this->getPaymentSettings();

    $build = [];
    if ($settings['block_rules_payment_system']) {
      $entity_type = 'block_content';
      $view_mode = 'default';
      $block = \Drupal::entityTypeManager()->getStorage($entity_type)->load($settings['block_rules_payment_system']);
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $build = $view_builder->view($block, $view_mode);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUserBalance(float $order_cost) {
    $balance = \Drupal::service('site_commerce_balance.controller')->getBalance();
    $number = \Drupal::service('kvantstudio.formatter')->removeTrailingZeros($balance->getBalanceNumber());
    $result = bccomp($number, $order_cost, 2);
    if ($result >= 0) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUserBalance(array $payment_data, string $type = 'write_off') {

    $balance = \Drupal::service('site_commerce_balance.controller')->getBalance($payment_data['uid'], 'currency_account', $payment_data['currency']);

    // Текущий баланс на счете.
    $number_current = $balance->getBalanceNumber();

    // Если выполняется списание средств со счета.
    if ($type == 'write_off') {
      $number_update = (float) ($number_current - $payment_data['number']);
    }

    // Если выполняется начисление средств на счет.
    if ($type == 'refill') {
      $number_update = (float) ($number_current + $payment_data['number']);
    }

    // Проверка, что остаток баланса больше или равен нулю.
    if (bccomp($number_update, 0, 10) >= 0) {
      $balance->setBalanceNumber($number_update);
      $balance->save();

      return $balance;
    }

    return FALSE;
  }

}
